#!/bin/bash
# Install Wavefront Proxy and configures standard collectd plugin
# ####

function logo() {
    cat << "EOT"
 __      __                     _____                      __   
/  \    /  \_____ ___  __ _____/ ____\______  ____   _____/  |_ 
\   \/\/   /\__  \\  \/ // __ \   __\\_  __ \/  _ \ /    \   __\
 \        /  / __ \\   /\  ___/|  |   |  | \(  <_> )   |  \  |  
  \__/\  /  (____  /\_/  \___  >__|   |__|   \____/|___|  /__|  
       \/        \/          \/                         \/      
EOT
}

curl -s https://packagecloud.io/install/repositories/wavefront/telegraf/script.rpm.sh | sudo bash
yum install telegraf -y

cat > /etc/telegraf/telegraf.d/10-wavefront.conf <<EOF
[[outputs.wavefront]]
 host = "wavefrontcp.slatchdev.local"
 port = 2878
 metric_separator = "."
 source_override = ["hostname", "agent_host", "node_host"]
 convert_paths = true
EOF
